# Zookeeper role
## Use
```bash
# clone repo
git clone git@gitlab.com:kapuza-ansible/zookeeper.git
cd zookeeper

# come to example
cd example

# Edit inventory/hosts
vim inventory/hosts

mkdir inventory/group_vars
cp ../defaults/main.yml inventory/group_vars/zookeeper.yml

# Edit configs
vim inventory/group_vars/zookeeper.yml

# Install
ansible-playbook 10-zookeeper_install.yml

# Configure and restart
ansible-playbook 20-zookeeper_configure.yml
```
